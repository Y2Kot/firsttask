package ru.maxim.firsttask;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;

public class NumberRepresentationTest {

    @Test
    public void checkNumbers() {
        try {
            Assert.assertEquals(new NumberRepresentation("one").getNumeric(), 1);
            Assert.assertEquals(new NumberRepresentation("one").getNumeric(), 1);
            Assert.assertEquals(new NumberRepresentation("one hundred").getNumeric(), 100);
            Assert.assertEquals(new NumberRepresentation("nine thousand and twenty").getNumeric(), 9020);
            //Assert.assertEquals(new NumberRepresentation("nine thousand and twenty").getNumeric(), 1);
            Assert.assertEquals(new NumberRepresentation("three hundred and twenty five").getNumeric(), 325);
            Assert.assertEquals(new NumberRepresentation("one thousand five hundred and seventy five").getNumeric(), 1575);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
