package ru.maxim.firsttask;

import java.util.ArrayList;
import java.util.Comparator;

public class SecondThread extends Thread {
    private volatile ArrayList<NumberRepresentation> list;

    SecondThread(ArrayList<NumberRepresentation> list) {
        this.list = list;
        start();
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                Thread.sleep(5000);
                printAndRemove();
            } catch (IndexOutOfBoundsException e) {
                System.err.println("Обращение за пределы массива");
            } catch (InterruptedException e) {
                System.err.println("Прервано");
            }
        }
    }

    private synchronized void printAndRemove() {
        if (list.size() != 0) {
            list.sort(Comparator.comparingInt(NumberRepresentation::getNumeric));
            if (list.get(0).getNumeric() == -1)
                interrupt();
            else {
                System.out.println("\nМинимальное значение: " + list.get(0).getText());
                list.remove(0);
            }
        }
    }
}
