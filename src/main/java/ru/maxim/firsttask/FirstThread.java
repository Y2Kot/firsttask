package ru.maxim.firsttask;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;

public class FirstThread extends Thread {
    private volatile ArrayList<NumberRepresentation> list;
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    FirstThread(ArrayList<NumberRepresentation> list) {
        this.list = list;
        start();
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            String inputString = "";
            try {
                System.out.print("Введите число: ");
                inputString = reader.readLine();
                if (inputString.equalsIgnoreCase("exit"))
                    interrupt();
                list.add(new NumberRepresentation(inputString));
            } catch (ParseException e) {
                if (!inputString.equalsIgnoreCase("exit"))
                    System.err.println("Вероятно введено некорректное число");
            } catch (IOException e) {
                System.err.println("Непредвиденная ошибка");
            }
        }
    }
}
