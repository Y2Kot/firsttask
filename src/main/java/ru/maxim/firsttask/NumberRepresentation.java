package ru.maxim.firsttask;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

public class NumberRepresentation {
    private static Map<String, Integer> validPairs = new HashMap<String, Integer>() {{
        put("one", 1);
        put("two", 2);
        put("three", 3);
        put("four", 4);
        put("five", 5);
        put("six", 6);
        put("seven", 7);
        put("eight", 8);
        put("nine", 9);
        put("ten", 10);
        put("eleven", 11);
        put("twelve", 12);
        put("thirteen", 13);
        put("fourteen", 14);
        put("fifteen", 15);
        put("sixteen", 16);
        put("seventeen", 17);
        put("eighteen", 18);
        put("nineteen", 19);
        put("twenty", 20);
        put("thirty", 30);
        put("forty", 40);
        put("fifty", 50);
        put("sixty", 60);
        put("seventy", 70);
        put("eighty", 80);
        put("ninety", 90);
        put("hundred", 100);
        put("thousand", 1000);
    }};
    private String text;
    private int numeric;

    public NumberRepresentation(String text) throws ParseException {
        this.text = text;
        this.numeric = initNumeric();
        if (this.numeric == 0)
            throwException();
    }

    private int initNumeric() throws ParseException {
        int result = 0;
        int total = 0;

        if (text == null || text.isEmpty())
            throwException();
        String[] slicedText = text.toLowerCase()
                .replaceAll(" and ", " ")
                .trim()
                .split("\\s+");
        for (String str : slicedText) {
            if (!validPairs.keySet().contains(str)) {
                if (str.equalsIgnoreCase("exit"))
                    return -1;
                else throwException();
            }
            for (Map.Entry<String, Integer> entry : validPairs.entrySet()) {
                if (str.equalsIgnoreCase(entry.getKey())) {
                    if (entry.getValue() < 100)
                        result += entry.getValue();
                    else if (entry.getValue() == 100)
                        result *= 100;
                    else if (entry.getValue() == 1000) {
                        result *= 1000;
                        total += result;
                        result = 0;
                    }
                }
            }
        }
        total += result;
        return total;
    }

    public String getText() {
        return text;
    }

    public int getNumeric() {
        return numeric;
    }

    private void throwException() throws ParseException {
        throw new ParseException("Ошибка при конвертации строки в число", 0);
    }
}