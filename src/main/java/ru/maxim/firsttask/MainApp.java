package ru.maxim.firsttask;

import java.util.ArrayList;

public class MainApp {
    private static volatile ArrayList<NumberRepresentation> list = new ArrayList<>();

    public static void main(String[] args) {
        new FirstThread(list);
        new SecondThread(list);
    }
}
